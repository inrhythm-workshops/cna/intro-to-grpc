# Intro to gRPC with Java


## Purpose
The purpose of this repo is to introduce you to creating APIs using gRPC.

## Pre-Requisites
Please ensure you have the following software tools installed on your computer:
* IntelliJ IDEA or similar IDE for editing Java files
* Java OpenJDK 17 or higher 
  * These labs were tested with version: `openjdk 17.0.6 2023-01-17`
  * The code in our repo uses Spring 3.0.x which requires Java 17
  * We recommend OpenJDK 17 from  https://adoptium.net/temurin/releases/
* Maven
  * Windows: Download from https://maven.apache.org/download.cgi, and follow installation directions from here: https://mkyong.com/maven/how-to-install-maven-in-windows/
  * MacOS:  `brew install maven`
  * Linux:  `sudo apt-get maven`


### Optional Pre-Requisite
If you wish to run a local instance of Postgres, then we recommend installing Docker .
* Docker - download the docker desktop here https://www.docker.com/products/docker-desktop/
  * For Docker Desktop, make sure you have version 4.0 or higher.  For Docker CLI, you want version 20 or higher.
  * From the command line, you can verify your CLI version by running the command: `docker --version`

 ---

### Optional Setup

#### gRPC GUI
You may want to install a GUI tool to make calls to our gRPC server. 
Though not necessary, you may prefer to have an external tool connect and verify and diagnose connections. 

We recommend that you use [Kreya](https://kreya.app/downloads/) or similar such as [Insomnia](https://insomnia.rest/) or [Postman](https://www.postman.com/api-platform/api-client/).


#### Protocol Buffer Compiler
In our workshop we will configure Maven to compile protocol buffer files for us. However, if you prefer, you can install an OS specific command line compiler.
* Protocol Buffer Compiler Installation details: https://grpc.io/docs/protoc-installation/
  * Windows: https://github.com/protocolbuffers/protobuf/releases
  * MacOS: `brew install protobuf` or download from the link above
  * Linux: `sudo apt install protobuf-compiler`
  * Ensure you have version 3+ by typing `protoc --version` in your command line

---

### Verify your version of Java
From your command line check your Java version: `java --version`

>**NOTE:** We have validated this code base against OpenJDK 17.06 from Adoptium.net. We have configured the maven pom.xml to require Java 8 or higher.

Be sure to configure your IDE to use this version of the Java JDK for this project.

## Getting Started
Once you have installed the prerequisite applications and verified you version of Java, you should be ready to go. 

### Clone the Dev branch of Repo
If you have not already done so, either clone or download a copy of the `dev` branch.
This should give you a good place to start with a _user_ and _note_ DAO service and entities.

If you get stuck, or want to see the completed workshop, simply clone or download the `completed` branch.



### Docker Database Setup
> NOTE: If you wish to use another database instance, please edit the application.properties to define the connection.

Lets make sure you have Docker running. Open up a command line terminal and go to the root of your project file.

From to command line, run the command below. This will download and spin up an instance of Postgres database server, and run it in the background (detached mode).
```
docker compose up db --build -d
```

#### Configure DB Connection
Open the file `application.properties`
* Comment out the section labeled `Example AWS - Postgres Config`
* Uncomment the section labeled `LOCAL - Postgres Config`


#### Run Flyway Migrations
>NOTE: Only do this if running a local db
> 
Next run the following Docker compose command to run data migration scripts with Flyway.
```
docker compose up flyway --build
```
The results should look similar to this:
![image tne-flyway-db-up.png](./assets/screenshots/tne-flyway-db-up.png)


#### Optional: Configure IntelliJ to connect to the database instance
To connect the database to a tool such as IntelliJ's Database connector, see
[Configure IntelliJ DB Access](./assets/configure-intellij-db-access.md)



## Verify your local setup
Import the project into your favorite IDE. We recommend IntelliJ.

Once you have pulled the project into your IDE, please do the following:
* Run Maven clean and compile (either from your IDE or command line).
* Open up the jUnit Integration Test: `UserNoteIntegrationTest` and run it.
  * It the tests pass, you have successfully read, written and deleted a record in the database!
  * If the tests fail, be sure that your database is up and running.

> FYI: If you look at the details of the Integration Test you will see that we add and remove a few test Users.


## Workshops:
Now that your environment setup is complete, you are ready to move on to coding exercises.
* [Lab 1: Getting Started with Protocol Buffers](./assets/lab1-protobuf.md)
* [Lab 2: Getting Started with gRPC](./assets/lab2-grpc.md)

