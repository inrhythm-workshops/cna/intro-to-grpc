package com.example.tntengine;

import com.example.tntengine.common.NoteType;
import com.example.tntengine.jpa.entity.Note;
import com.example.tntengine.jpa.repo.NoteRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class NoteDAO {
    private static Logger logger = LoggerFactory.getLogger(NoteDAO.class);

    @Autowired
    private NoteRepo noteRepo;


    public Note createNote(String note, NoteType noteType, UUID userId) {
        Note newNote = new Note(note, noteType, userId);
        noteRepo.save(newNote);
        return newNote;
    }

   public boolean updateNote(Note note) {
        boolean bSuccess = true;

        try {
            note.setDateUpdated(LocalDate.now());
            noteRepo.save(note);
        }  catch (Exception ex) {
            logger.error("Failed to save Note to the database. \n{}", ex.getMessage());
            bSuccess = false;
        }

        return bSuccess;
   }

   public boolean deleteNote(Note note) {
        try {
            noteRepo.delete(note);
        } catch (Exception ex) {
            logger.error("Failed to delete Note. \n{}", ex.getMessage());
            return false;
        }

        return true;
   }

   public Note getNoteById(UUID noteId) {
       Optional<Note> myNote = noteRepo.findById(noteId);

       return myNote.isPresent() ? myNote.get() : null;
   }

    public List<Note> getNotesByUserId(UUID userId) {
        return noteRepo.findAllNotesByUSerId(userId);
    }

}
