package com.example.tntengine;

import com.example.tntengine.common.UserType;
import com.example.tntengine.jpa.entity.User;
import com.example.tntengine.jpa.repo.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserDAO {
    private static Logger logger = LoggerFactory.getLogger(UserDAO.class);

    @Autowired
    private UserRepo userRepo;

    /**
     * Return a list of all users
     * @return
     */
    public List<User> getAll() {
        return userRepo.findAll();
    }

    /***
     * Get user by UserId
     * @param userId
     * @return
     */
    public User getUserById(UUID userId) {
        return userRepo.findUserById(userId);
    }

    /***
     * Return records with last name starting with the value passed in
     * @param lastName
     * @return
     */
    public List<User> getUsersByLastName(String lastName) {
        return userRepo.findUsersByLastName(lastName);
    }

    /**
     * Lookup user by username
     * @param userName
     * @return
     */
    public User getUserByUsername(String userName) {
        return userRepo.findByUserName(userName);
    }

    public List<User> getUserByType(UserType userType) {
        return userRepo.findUsersByType(userType.getCode());
    }

    /***
     * Create a user record, if it does not already exist, based by username
     * @param userName
     * @param firstName
     * @param lastName
     * @param userType
     * @return
     */
    public User createUser(String userName, String firstName, String lastName, UserType userType) {
        // Check to see if the username already exists
        User existingUser = userRepo.findByUserName(userName);

        if (existingUser == null) {
            // Create user obj
            User newUser = new User(userName, firstName, lastName, userType);
            // persist to db
            userRepo.save(newUser);
            return newUser;
        }

        return existingUser;
    }

    public boolean deleteUser(User user) {
        boolean success = true;

        if (user != null) {
            try {
                userRepo.delete(user);
            } catch (Exception ex) {
                success = false;
                logger.info("Unable to delete the specified user.");
            }
        }

        return success;
    }
}
