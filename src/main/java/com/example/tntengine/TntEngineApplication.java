package com.example.tntengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;
import org.springframework.core.env.*;


@SpringBootApplication
public class TntEngineApplication {
    private static final Logger logger = LoggerFactory.getLogger(TntEngineApplication.class);
    private static int GRPC_SERVER_PORT = 9090;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private Environment env;


    public static void main(String[] args) {
        SpringApplication.run(TntEngineApplication.class, args);
    }

    /** Declare our Beans **/
    @Profile("!test")
    @Bean
    public CommandLineRunner demo() {

        return (Args) -> {
            logger.info("-------------------------------");
            logger.info("-- Welcome to the Note Taker --");
            logger.info("-------------------------------");

            // Check to see if we have a custom port defined
            int grpcPort = env.getProperty("grpc.server.port", Integer.class, GRPC_SERVER_PORT);
            logger.info("gRPC server port is: {}", grpcPort);

            // TODO: Initialize our gRPC Server here


            logger.info("\b------------------------------");
       };

    }


}
