package com.example.tntengine.common;

public enum NoteType {
    NOTE ("NOTE", "Note", "A note."),
    REMINDER ("REMINDER", "Reminder", "A reminder to do/complete something."),
    TASK ("TASK", "Task", "A task to complete.");


    // Data Type matches with table ddTypes.data_type
    private final static String dataType = "NOTE";
    private final String code;
    private final String name;
    private final String description;

    private NoteType(String code, String name, String description) {
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public static String getDataType() {
        return dataType;
    }
}
