package com.example.tntengine.common;

public enum UserType {
    USER ("USER", "User", "User"),
    CSR("CSR", "CSR", "Customer Service Rep"),
    HERO("HERO", "Hero", "Super Hero"),
    VILLAIN("VILLAIN", "Villain", "Chilling like a villain."),
    ADMIN("ADMIN", "Admin", "Admin User"),
    DEV ("DEV", "Dev", "Development User");

    private final static String dataType = "USER";
    private final String code;
    private final String name;
    private final String description;

    private UserType(String code, String name, String description) {
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public String getDataType() {
        return this.dataType;
    }

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

}
