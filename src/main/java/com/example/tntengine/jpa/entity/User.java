package com.example.tntengine.jpa.entity;

import com.example.tntengine.common.UserType;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.UUID;

@Entity(name = "users")
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="user_id")
    private UUID id ;
    @Column(name="user_name", nullable = false)
    private String userName;
    @Column(name="name_first")
    private String firstName;
    @Column(name="name_last")
    private String lastName;

    @Column(name="user_type")
    private String type;

    @Column(name="date_created")
    private LocalDate dateCreated;
    @Column(name="date_updated")
    private LocalDate dateUpdated;

    public User() {}

    public User(String userName, String firstName, String lastName, UserType userType) {
        setUserName(userName);
        setFirstName(firstName);
        setLastName(lastName);
        setType(userType);

        this.dateCreated = LocalDate.now();
        this.dateUpdated = LocalDate.now();
    }

    public UUID getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        // Replace spaces with underscores
        this.userName = userName.trim().replaceAll(" ", "_");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName.trim();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.trim();
    }

    public UserType getType() {
        return UserType.valueOf(type);
    }

    public void setType(UserType type) {
        this.type = type.getCode();
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public LocalDate getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDate dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public String toString() {
        return String.format("User { id:'%s', userName='%s', firstName='%s', lastName='%s', type='%s' }",
                id, userName, firstName, lastName, type);
    }
}
