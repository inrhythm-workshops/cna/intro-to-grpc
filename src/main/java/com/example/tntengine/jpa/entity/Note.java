package com.example.tntengine.jpa.entity;

import com.example.tntengine.common.NoteType;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.UUID;

@Table(name = "notes")
@Entity(name = "notes")
public class Note {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="note_id", updatable = false)
    private UUID id;

    @Column(name="note", nullable = false)
    private String note;

    // Input and output will be to DataType: NoteType, but for the database we store the string key
    @Column(name="note_type", nullable = false)
    private String noteType;

    @Column(name="user_id", nullable = false)
    private UUID userId;

    @Column(name="date_created", nullable = false, updatable = false)
    private LocalDate dateCreated = LocalDate.now();
    @Column(name="date_updated")
    private LocalDate dateUpdated = LocalDate.now();

    private Note() { }

    public Note(String note, NoteType noteType, UUID userId) {
        this.note = note.isEmpty() ? null : note.trim();
        this.noteType = noteType.getCode();
        this.userId = userId;
    }

    public UUID getId() {
        return id;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getNote() {
        return note;
    }

    public NoteType getNoteType() {
        return NoteType.valueOf(noteType);
    }

    public LocalDate getDateUpdated() {
        return dateUpdated;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }


    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public void setDateUpdated(LocalDate dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public String toString() {
        return String.format("Note { id:'%s', userId='%s', noteType='%s', note='%s' }",
                getId(), getUserId().toString(), getNoteType(), getNote() );
    }

}
