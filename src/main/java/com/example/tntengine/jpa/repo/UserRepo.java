package com.example.tntengine.jpa.repo;

import com.example.tntengine.jpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepo extends JpaRepository<User, UUID> {
    // Lookup User by UserName
    @Query(value = "SELECT * FROM users u WHERE LOWER(u.user_name) = LOWER(:userName) order by u.date_created desc fetch first 1 rows only", nativeQuery = true)
    User findByUserName(@Param("userName") String userName);

    // Lookup User by User ID
    @Query(value = "SELECT * FROM users u WHERE u.user_id = :id", nativeQuery = true)
    User findUserById(UUID id);

    // Lookup Users by last name.
    @Query(value = "SELECT * FROM users u WHERE LOWER(u.name_last) = LOWER(:lastName) ", nativeQuery = true)
    List<User> findUsersByLastName(@Param("lastName") String lastName);

    @Query(value = "SELECT * FROm users u WHERE u.user_type = :userType", nativeQuery = true)
    List<User> findUsersByType(String userType);
}
