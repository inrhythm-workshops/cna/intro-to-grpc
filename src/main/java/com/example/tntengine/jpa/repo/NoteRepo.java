package com.example.tntengine.jpa.repo;

import com.example.tntengine.jpa.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface NoteRepo extends JpaRepository<Note, UUID> {

    // Lookup Notes by User Id
    @Query(value = "SELECT * FROM notes n WHERE n.user_id = :userId", nativeQuery = true)
    List<Note> findAllNotesByUSerId(@Param("userId") UUID userId);
}
