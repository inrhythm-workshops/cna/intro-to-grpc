package com.example.tntengine;

import com.example.tntengine.common.NoteType;
import com.example.tntengine.common.UserType;
import com.example.tntengine.jpa.entity.Note;
import com.example.tntengine.jpa.entity.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.test.context.*;
import org.springframework.util.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
public class UserNoteIntegrationTest {
    private static final Logger logger = LoggerFactory.getLogger(UserNoteIntegrationTest.class);
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private NoteDAO noteDAO;

    private final Map<String, User> heroeMap = new HashMap<>();
    private User jackJack;
    private User dash;

    @BeforeAll
    public void init() {
        Assert.notNull(userDAO, "UserService should not be null, unable to continue.");
        Assert.notNull(noteDAO, "NoteDAO should not be null, unable to continue.");

        if (jackJack == null) {
            jackJack = userDAO.createUser("jack jack", "Jack", "Parr", UserType.HERO);
            Assert.notNull(jackJack, "User object jackjack should not be null");
            heroeMap.put("jackjack", jackJack);
        }

        if (dash == null) {
            dash = userDAO.createUser("dash", "Dash", "Parr", UserType.HERO);
            Assert.notNull(dash, "User object dash should not be null");
            heroeMap.put("dash", dash);
        }
    }

    @Test
    public void createUser() {
        logger.info("Welcome to the createUser test");

        Assert.notEmpty(heroeMap, "HeroMap should not be empty, the init() should have populated.");
        logger.info("Total Heroes: {}", heroeMap.size());

        logger.info("Total number of users: {}",  userDAO.getAll().size());

        // Add Frozone to our hero map
        User frozone =  userDAO.createUser("Frozone", "Lucius", "Bust", UserType.USER);
        Assert.notNull(frozone,"frozone user object should not be null.");
        logger.info("Adding {} to our list of heroes.", frozone.getUserName());
        heroeMap.put(frozone.getUserName(), frozone);


        logger.info("Total Heroes: {}", heroeMap.size());

        heroeMap.forEach((k, o) -> logger.info("key: {}, value: {}", k, o.toString()));
    }

    @Test
    public void createNote() {
        logger.info("Attempting to add notes to user: {}.", jackJack.getUserName());
        logger.info("{} has {} notes.", jackJack.getFirstName(), noteDAO.getNotesByUserId(jackJack.getId()).size());

        // Attempt to add a cookie to JackJack
        Note reminderToGetCookies = noteDAO.createNote("Get more Nom-Nom cookies!", NoteType.REMINDER, jackJack.getId());
        Assert.notNull(reminderToGetCookies, "reminderToGetCookies reminder note should not be null.");

        List<Note> jackJacksNotes = noteDAO.getNotesByUserId(jackJack.getId());
        logger.info("{} now has {} notes.", jackJack.getFirstName(), jackJacksNotes.size());
        for (Note note : jackJacksNotes) {
            logger.info(note.toString());
        }


        // Attempt to delete note
        boolean deletedReminder = noteDAO.deleteNote(reminderToGetCookies);
        Assert.isTrue(deletedReminder, "deletedReminder should be TRUE, indicating reminder/note is deleted.");

        logger.info("{} now has {} notes.", jackJack.getFirstName(), noteDAO.getNotesByUserId(jackJack.getId()).size());
    }


    @AfterAll
    public void tearDown() {
        logger.info("Removing test entries");

        for (User user : heroeMap.values()) {
            if (user != null) {
                String userName = user.getUserName();
                logger.info("Attempting to delete user, {}, from the database.", userName);
                if (userDAO.deleteUser(user)) {
                    logger.info("Deleted user: {}", userName);
                } else {
                    logger.info("Failed to delete user, {}, from the database.", userName);
                }
            }
        }

        logger.info("All test heroes have been removed from the database.");
        logger.info("Total number of users is now: {}",  userDAO.getAll().size());
    }
}
