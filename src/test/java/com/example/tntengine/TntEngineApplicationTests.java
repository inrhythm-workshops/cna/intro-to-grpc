package com.example.tntengine;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.*;

@ActiveProfiles("test")
@SpringBootTest
class TntEngineApplicationTests {

    @Test
    void contextLoads() {
        // This is a stub... we will  complete the test in Lab 1
    }

}
