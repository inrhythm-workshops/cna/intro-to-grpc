/*
 * Populate our Data Definition Table with NOTE data types
 */

-- Create a temp table of our user_types
CREATE TEMPORARY TABLE temp_note_types(
  dd_type varchar(25) not null,
  type_key varchar(20) not null,
  type_name varchar(50) not null,
  type_description varchar(255) null,
  date_created timestamp default current_timestamp,
  date_updated timestamp null,
  PRIMARY KEY (dd_type, type_key)
);

/*
 Load the temporary tale with required Note Types
 */
INSERT INTO temp_note_types(dd_type, type_key, type_name, type_description, date_created)
VALUES('NOTE', 'NOTE', 'Note', 'Create a note', '2023-03-01');

INSERT INTO temp_note_types(dd_type, type_key, type_name, type_description, date_created)
VALUES('NOTE', 'REMINDER', 'Reminder', 'Set a reminder', '2023-03-01');

INSERT INTO temp_note_types(dd_type, type_key, type_name, type_description, date_created)
VALUES('NOTE', 'TASK', 'Task', 'Create a task to complete', '2023-03-01');


/*
 Load user types into dd_types if they do not exist
 */
INSERT INTO dd_types(dd_type, type_key, type_name, type_description, date_created)
SELECT tut.dd_type, tut.type_key, tut.type_name, tut.type_description, tut.date_created
FROM temp_note_types tut
         LEFT OUTER JOIN dd_types dt on tut.dd_type = dt.dd_type and tut.type_key = dt.type_key
WHERE 1=1
GROUP BY tut.dd_type, tut.type_key, tut.type_name, tut.type_description, tut.date_created
HAVING count(dt.type_key) = 0
;

-- Drop our temp table, we don't want to leave a mess!
DROP TABLE temp_note_types;
