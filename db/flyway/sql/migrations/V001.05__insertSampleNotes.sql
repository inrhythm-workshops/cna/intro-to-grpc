/** Script to insert a sample note for our sample users  **/

insert into notes (note_id, note, note_type, user_id, date_created, date_updated)
values ('74e19ba5-a2c2-4714-8aa0-986ecf49892f', 'Not Alone Book series by Craig Falconer.', 'NOTE', '0a8672ff-e452-409e-b7c4-ae0a6f2bd371', '2023-03-01', '2023-03-01'),
       ('f94b5fbf-ccda-443a-bd48-fbe80c6689c8', 'The Dark Tower series by Stephen King.', 'NOTE', '290f36c5-0b70-402a-849c-36ee18b6c36a', '2023-03-01', '2023-03-01'),
       ('8b18dd17-f8e8-4560-87f5-66b8ed326f38', 'The Joe Ledger series by Jonathan Maberry.', 'NOTE', 'fd0d6748-f34c-4591-8271-031d5e95cac8', '2023-03-01', '2023-03-01'),
       ('7173e826-4f7f-4a28-a559-ba19d9012c1c', 'Tales of Fred, the Vampire by Drew Hayes.', 'NOTE', '8aa549a4-90e3-463f-97ab-e359711e00a2',  '2023-03-01', '2023-03-01'),
       ('36ad0abb-efb8-4d18-93f4-4e865064ec08', 'Dune prequel: Prelude to Dune series by Brian Herbert and Kevin J Anderson.', 'NOTE', '88ada5c9-68a4-45c7-99bf-361a12477b48', '2023-03-01', '2023-03-01')
;

/** Add a Task note type to two of our records **/
insert into notes (note_id, note, note_type, user_id, date_created, date_updated)
values ('7af7a86f-3c3f-414b-8833-1604c1e07086', 'Where or where is the turtle and the bear?', 'TASK', '290f36c5-0b70-402a-849c-36ee18b6c36a', '2023-03-10', '2023-03-12'),
       ('a2e919fb-4ede-4955-b464-104c94b53b95', 'Fred''s adventures take place in Winslow, Colorado. Check to see if is a real place.', 'TASK', '8aa549a4-90e3-463f-97ab-e359711e00a2',  '2023-03-02', '2023-03-04'),
       ('7ec300e4-8e42-496d-8754-80bee9299851', 'Check background notes to see if there is any chance that Duncan is distantly related to Vorian.', 'TASK', '88ada5c9-68a4-45c7-99bf-361a12477b48', '2023-03-03', '2023-03-03')
;