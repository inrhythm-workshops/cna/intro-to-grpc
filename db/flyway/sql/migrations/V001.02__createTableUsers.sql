/* Script to create our users table */
CREATE TABLE users (
    user_id uuid not null primary key,
    user_name varchar(100) not null,
    name_first varchar(50) not null,
    name_last varchar(50) not null,
    user_type varchar(20) not null,
    date_created timestamp default current_timestamp,
    date_updated timestamp null
);
