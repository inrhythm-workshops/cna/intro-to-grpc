/*
  Create data definition table to store data types
  Data definition is similar in nature to an enum, but is stored data driven rather, so that types could be
  added/updated without requiring a re-deploy

  dd_type = data definition type (or group). i.e. color, user,
  type_key = the key value. For instance if the dd_type is "color" the type_key could be "blue". Think of this as
    the short value or what you would want to store in a table
  type_name = a descriptive name for the data type. Often this would be displayed to the end-users
  type_description = (optional) store a description of the key, again often for display
 */
 CREATE TABLE dd_types(
     dd_type varchar(25) not null,
     type_key varchar(20) not null,
     type_name varchar(50) not null,
     type_description varchar(255) null,
     date_created timestamp default current_timestamp,
     date_updated timestamp null,
     PRIMARY KEY (dd_type, type_key)
 );