CREATE TABLE notes (
    note_id uuid not null,
    note text,
    note_type varchar(20),
    user_id uuid not null,
    date_created timestamp default current_timestamp,
    date_updated timestamp null,
    PRIMARY KEY (note_id),
    CONSTRAINT fk_users FOREIGN KEY (user_id) REFERENCES users(user_id)
);
