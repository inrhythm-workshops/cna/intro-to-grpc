/* Script to create sample users */
INSERT INTO users (user_id, user_name, name_first, name_last, user_type, date_created)
VALUES ('0a8672ff-e452-409e-b7c4-ae0a6f2bd371', 'Not_Alone', 'Craig', 'Falconer', 'USER', '2023-03-01'),
    ('290f36c5-0b70-402a-849c-36ee18b6c36a', 'The_Gunslinger', 'Stephen', 'King', 'USER', '2023-03-01'),
    ('fd0d6748-f34c-4591-8271-031d5e95cac8', 'Captain_Ledger', 'Jonathan', 'Maberry', 'USER', '2023-03-01'),
    ('8aa549a4-90e3-463f-97ab-e359711e00a2', 'Fred_Fletcher', 'Drew', 'Hayes', 'USER', '2023-03-01'),
    ('88ada5c9-68a4-45c7-99bf-361a12477b48', 'Duncan_Idaho', 'Brian', 'Anderson', 'USER', '2023-03-01')
;

