# Lab 2: Hands on gRPC

In Lab 1 we defined a proto file for Users and Notes, added in our dependencies, and used the gRPC compiler to compile the proto files into Java. 

In **Lab 2** we will create implementations of the generated gRPC files and link them to our data access objects (DAOs). We will create:
* gRPC Server Service
* User & Note Service Implementation
* User & Note Client
* A User Client integration test


## Step 1: Setup
> If you have not completed Lab 1 yet, please go back and do so, or download the Lab 1 branch from our repo at 
> https://gitlab.com/inrhythm-workshops/cna/intro-to-grpc/-/tree/lab1

Let's create a new branch for lab 2, from lab 1:

```bash
# Assuming you are on lab1
git checkout -b lab2
``` 

## Step 2: Implement the User Service
To get started lets first:
* Create a new package: `grpc.server`
* Create a Java class called `UserSrvcImpl.java`

Once you start editing your file, you want to **extend** the `UserSrvcGrpc.UserSrvcImplBase` from generated sources.
> NOTE: If IntelliJ does not see your Generated Sources as being available to your project, do the following:
> 
> * Open menu bar "File > Project Structure > Modules"
> * Expand the "Target" folder 
> * Click on "generated-sources" and then up above it click on the tab that says "Source" to mark your folder as included. 
> * Click the "Apply" button and exit the pop-up window.

In our file we will want to create a custom implementation of the methods:
* getAllUsers
* getUsersByUsername

We'll start our file off by extending `UserSrvcGrpc.UserSrvcImplBase`, and adding in a logger, our UserDAO, and a constructor.

```java
@GrpcService
public class UserSrvcImpl extends UserSrvcGrpc.UserSrvcImplBase {
    private static final Logger logger = LoggerFactory.getLogger(UserSrvcImpl.class);
    private UserDAO userDAODAO;

    public UserSrvcImpl(UserDAO userDAO) {
        this.userDAODAO = userDAO;
    }
```


ProtoBufs do not have a date format. To account for this you may recall we defined our dateCreated and dateUpdated fields as a Long.  So we will create a method called `getEpochDay()` to help us account for this. Since our database allows for the dateUpdated column to be NULL, this method will check perform a NULL check and substitute a default date if a NULL is found, and it will return the default Long value.

```java
/**
 * Method to return the epochDay of either a Target Date or a Default Date.
 * @param targetDate
 * @param defaultDate
 * @return
 */
private long getEpochDay(LocalDate targetDate, LocalDate defaultDate) {
    if (defaultDate == null) {
        defaultDate = LocalDate.now();
    }

    if (targetDate == null) {
        targetDate = defaultDate;
    }

    return targetDate.toEpochDay();
}
```


As you may recall, in our `grpc_user_srvc.proto` file we defined a service called `getAllUsers`. Let's create a custom implementation
of that here. In this example we will return a unary response, much like a traditional REST response. We will pass in a request in protobuf format
and create a StreamObserver for our response.

In this method, we start by creating an instance of the GetAllUsersResponse.Builder.

We then loop over query results from `UserDAO.getAll()` method, and use that to create a UserEnvelope for each
User record. We will then append this UserEnvelope to our response builder. 

To send the response back, we will place it in the Response Observer's **onNext()**

```'java
@Override
public void getAllUsers(GetAllUsersRequest request, StreamObserver<GetAllUsersResponse> responseObserver) {
    // Create an instance of our GetAllUsersResponse Builder
    GetAllUsersResponse.Builder responseBuilder = GetAllUsersResponse.newBuilder();

    // Loop over our Users and populate the userEnvelope
    for (User user : userDAODAO.getAll()) {
        // Create an envelope for the current user
        UserEnvelope thisUser = UserEnvelope.newBuilder()
                .setUserId(user.getId().toString())
                .setUserType(user.getType().getCode())
                .setUserName(user.getUserName())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setDateCreated( getEpochDay(user.getDateCreated(), null) )
                .setDateUpdated( getEpochDay(user.getDateUpdated(), user.getDateCreated()) )
                .build();

        // Append this user to our list of envelopes
        responseBuilder.addUserEnvelopes(thisUser);
    }

    // Complete building the response
    GetAllUsersResponse response = responseBuilder.build();

    // Prepare to return result
    responseObserver.onNext(response);
    responseObserver.onCompleted();
}
```

In the code above, you can see that we use two builders, one for the **Response** and one for the **UserEnvelope** our 
gRPC automatically generated these builder patterns for us.

Now let's create a method to lookup users by username.  Let's call this `getUserByUsername` and like above we will override the default
implementation. Here once again, we start a response builder, but unlike the method above we are also looking for details in the inbound request.

Provided we were provided a username, we will call the `UserDAO.getUserByUserName()` to lookup the user. If a user is found, we will build a UserEnvelope and return it via the Response Observer's onNext method. 

```javva
@Override
public void getUserByUsername(GetUserByUsernameRequest request, StreamObserver<GetUserByUsernameResponse> responseObserver) {
    GetUserByUsernameResponse.Builder responseBuilder = GetUserByUsernameResponse.newBuilder();

    if (request.getUserName().isEmpty()) {
        logger.info("No username provided f");
        // Return empty response
        responseObserver.onNext(responseBuilder.build());
        responseObserver.onCompleted();

    } else {
        User user = userDAODAO.getUserByUsername(request.getUserName());

        if (user != null) {

            responseBuilder.setUserEnvelope(
                    UserEnvelope.newBuilder().setUserId(user.getId().toString())
                            .setUserName(user.getUserName())
                            .setUserType(user.getType().getCode())
                            .setUserName(user.getUserName())
                            .setFirstName(user.getFirstName())
                            .setLastName(user.getLastName())
                            .setDateCreated( getEpochDay(user.getDateCreated(), null) )
                            .setDateUpdated( getEpochDay(user.getDateUpdated(), user.getDateCreated()) )
                            .build()
            ).build();

            // Append our response to the onNext
            responseObserver.onNext(responseBuilder.build());

        } else {
            String strError = "No record found in the database for user name, [" + request.getUserName() + "]";
            responseObserver.onError(new Throwable(strError) );
        }

        // Optional, populat the onCompleted
        responseObserver.onCompleted();
    }

}
```



## Step 3: User Integration Test 
Now that you have a User Service, we should add an integration test. Please download a copy 
of `UserServiceIntegrationTest` from our Lab2 Branch at https://gitlab.com/inrhythm-workshops/cna/intro-to-grpc/-/blob/lab2/src/test/java/com/example/tntengine/grpc/UserServiceIntegrationTest.java

Once you have this file added, run a Maven clean and compile to ensure everything builds correctly. 

Now select one of the two tests to run from your IDE. Each should write-out results to the run terminal.

>Note: Your database must still be up and running. If you have shut it down, please bring it back up.

## Next Step: Create the Notes Service
This lab continues on with [Setting up a gRPC Note Service Implementation](../assets/lab2-grpc-note-srvc.md)


---
### Challenge:
Feeling up for a challenge? Then attempt to create the file: `NoteSrvcImpl.java` on your own. 
Be sure to save it to the same location as the `UserSrvcImpl.java` in order to use it with this lab.

You can review the combination of the `grpc_note_srvc.proto`, the `NoteDAO`, and the User Service to get an ideal of how you should start.
