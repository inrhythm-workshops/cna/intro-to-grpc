# Lab 1: Getting started with Protocol Buffers

In gRPC we define our requests and responses in Protocol Buffer files. 
So we will start this lab off with building out a protocol buffer definition file for looking up  Users and Notes.

> Protocol Buffers are a language-neutral, platform-neutral extensible mechanism for serializing structured data.

One of the key things to keep in mind with proto buffers is that we specify the position each element is in. For instance if you were looking up a user by username, your request would likely have `username` defined as position 1.

The definitive source for all things related to Protocol Buffers (or ProtoBuf) see https://protobuf.dev/

For additional gRPC + Java and SpringBoot tutorials see: 
* https://protobuf.dev/getting-started/javatutorial/
* https://www.baeldung.com/grpc-introduction
* gRPC Spring Boot Starter: https://yidongnan.github.io/grpc-spring-boot-starter/en/
* ProtoBuf Style Guide: https://protobuf.dev/programming-guides/style/


***

## Define our User proto files
To use protobufs we must create a `.proto` file for our the item we want to encode/decode.
In our examples, we will store **proto** files in  `src/main/proto` and follow the recommended snake_case naming convention. 

## Step 1: Create grpc_user_srvc.proto
Create a file called: `grpc_user_srvc.proto` in the `src/main/proto` folder. 
In this file we will describe a set of services and messages, based on our User table, as described in our file `com.example.tntengine.jpa.entity.User` 


First we will indicate which ProtoBuff Syntax to use, the package, and set a few java options. In general, it is recommened that
the `package` and `java_package` be the same for better continuity. 

```protobuf
syntax = "proto3";

package com.example.tntengine.grpc.user;

option java_multiple_files = true;  // This will create a file for each RPC we define
option java_package = "com.example.tntengine.grpc.user";
```

## Step 2: Define a method to Look-up All Users
Now let's define a service to get all of our users. We will start off by defining the request, then the response, and finally the service.


```protobuf
/** Requests **/
message GetAllUsersRequest {
  // Intentionally empty request.  You might consider creating a generic version of this for re-use
  // For the purposes of our demo, this will simply return all users.
}

/** Responses **/
message GetAllUsersResponse {
  // indicate we will have multiple user envelopes, by using keyword repeated
  repeated UserEnvelope userEnvelopes = 1;
}

/** Objects **/
message UserEnvelope {
  string userName = 1;

  // For simplicity in our lab, we are treating UserType as a string. It is possible to use it as an enum.
  string userType = 2;

  optional string userId = 3;
  optional string firstName = 4;
  optional string lastName = 5;

  optional int64 dateCreated = 6;
  optional int64 dateUpdated = 7;
}

/** Service **/
service UserSrvc {
  rpc GetAllUsers (GetAllUsersRequest) returns (GetAllUsersResponse) {
  }
}
```
Let's dissect the messages above
#### Message: GetAllUsersRequest
No input required, so this has an empty message body.

#### Message: GetAllUsersResponse
We want to receive back a list (or collection) of users. So we will use the keyword `repeated` to indicate we will get more than one `UserEnvelope` returned to us.
We have placed indicated that we expect this list of user envelopes to be in position 1.
>Note: According to best practices document any item that returns multiple should have a plural name

#### Message: UserEnvelope
This is a message that describes what a User object (envelope) looks like, and in what position each of the user attributes will be in.

In this example we are saying that:

| Position  | Attribute |
|:---------:| --- |
|     1     | userName |
|     2     | userType |
|     3     | userId |
|     4     | firstName |
|     5     | lastName |
|     6     | dateCreated |
|     7     | dateUpdated |
  

#### Service: UserSrvc
Here we are defining our User Service. Inside the service definition we can define Remote Procedure Calls (rpc).
In our code above we have created a RPC to `GetAllUsers`, and in the parenthesis we indicate what the request looks like, and what it will return.


## Step 3: Define Lookup Users by Username
So now, lets add the following to our `grpc_user_srvc.proto` file to define a remote procedure call (rpc)  to get users by username.

```protobuf
// Add to the request section
message GetUserByUsernameRequest {
  string userName = 1;
}

// Add to our response section:
message GetUserByUsernameResponse {
  UserEnvelope userEnvelope = 1;
}

```

Then add a RPC to the Service section
```protobuf
service UserSrvc {
  rpc GetAllUsers (GetAllUsersRequest) returns (GetAllUsersResponse) {
  }

  rpc GetUserByUsername (GetUserByUsernameRequest) returns (GetUserByUsernameResponse) {
  }
}
```

To see the complete file, switch to branch `Lab1` in GitLab:
[grpc_user_srvc.proto](https://gitlab.com/inrhythm-workshops/cna/intro-to-grpc/-/blob/lab1/src/main/proto/grpc_user_srvc.proto)


## Step 4: Create Note Proto file
Create a file called: `grpc_note_srvc.proto` in the `proto` folder. This will define a *message* that we will send/receive Notes with.
This will be based on the fields in our Java class `jpa.entity.Note`

Let's start by copying the info in the codeblock below into our file. This defined remote procedure calls (rpc), one to get notes by ID and one to get notes by User ID.


```protobuf
syntax = "proto3";

package com.example.tntengine.grpc.notemodel;

option java_multiple_files = true;
option java_package = "com.example.tntengine.grpc.notemodel";


// Note Service
service NoteSrvc {

  // Get notes by note id
  rpc GetNoteById (GetNoteByIdRequest) returns (SingleNoteResponse) {
  }

  // Get Notes by User Id
  rpc GetNotesByUserId (GetNotesByUserIdRequest) returns (MultiNoteResponse) {
  }
}

/*
  Define message to get notes by Note Id
 */
message GetNoteByIdRequest {
  // This is a UUID in our database, simplest way to represent is with a string
  string id = 1;
}

message GetNotesByUserIdRequest {
  // UserId is a UUID in our Java app. Treated as a string for easier use with protobufs
  string userId = 1;
}

// Respond a single note
message SingleNoteResponse {
  NoteEnvelope noteEnvelope = 1;
}

// Respond with multiple notes
message MultiNoteResponse {
  repeated NoteEnvelope noteEnvelope = 1;
}


// Define a Note
message NoteEnvelope {
  optional string id = 1;  // This is a UUID in our database, simplest way to represent is with a string
  string note = 2;
  string noteType = 3;
  string userId = 4;  // This is a UUID in our database, simplest way to represent is with a string
  optional int64 dateCreated = 5;
  optional int64 dateUpdated = 6;
}
```

## Step 5: Compiling our ProtoBufs
Now that we have a couple of services and remote procedure calls defined, we need to generate code that we can use in our application.

### Configure Maven
Before we can compile, we need to add some items to our Maven POM. 

In the `<properties>` add the following:
```xml
    <spring.version>3.0.4</spring.version>
    <grpc.version>1.51.0</grpc.version>
    <protobuf.version>3.21.12</protobuf.version>
    <protobuf-plugin.version>0.6.1</protobuf-plugin.version>
```
Now let's add the following dependencies:
```xml
<!-- gRPC dependencies -->
<dependency>
    <groupId>io.grpc</groupId>
    <artifactId>grpc-netty-shaded</artifactId>
    <version>${grpc.version}</version>
</dependency>
<dependency>
    <groupId>io.grpc</groupId>
    <artifactId>grpc-protobuf</artifactId>
    <version>${grpc.version}</version>
</dependency>
<dependency>
    <groupId>io.grpc</groupId>
    <artifactId>grpc-stub</artifactId>
    <version>${grpc.version}</version>
</dependency>

<!-- necessary for Java 9+ and gRPC -->
<dependency>
    <groupId>org.apache.tomcat</groupId>
    <artifactId>annotations-api</artifactId>
    <version>6.0.53</version>
    <scope>provided</scope>
</dependency>

<!-- javax.annotation needed for some auto-generated items with a @Generated annotation. -->
<dependency>
    <groupId>javax.annotation</groupId>
    <artifactId>javax.annotation-api</artifactId>
    <version>1.3.2</version>
</dependency>
        
<!-- Spring Boot gRPC Server Starter -->
<dependency>
    <groupId>net.devh</groupId>
    <artifactId>grpc-server-spring-boot-starter</artifactId>
    <version>2.14.0.RELEASE</version>
</dependency>
        
<!-- Protobuf -->
<dependency>
    <groupId>com.google.protobuf</groupId>
    <artifactId>protobuf-java</artifactId>
    <version>${protobuf.version}</version>
</dependency>
```

In our `<build>` section we need to add a Extension to sniff out our OS and a Plugin to compile our ProtoBuff files. When done, your entire *build* section should look like:
```xml
<build>
    <extensions>
        <!-- Lab: This extension helps sniff our OS info -->
        <extension>
            <groupId>kr.motd.maven</groupId>
            <artifactId>os-maven-plugin</artifactId>
            <version>1.7.1</version>
        </extension>
    </extensions>

    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>

        <!-- Lab: Add this plugin in for protobufs -->
        <plugin>
            <groupId>org.xolstice.maven.plugins</groupId>
            <artifactId>protobuf-maven-plugin</artifactId>
            <version>${protobuf-plugin.version}</version>
            <configuration>
                <protocArtifact>com.google.protobuf:protoc:${protobuf.version}:exe:${os.detected.classifier}</protocArtifact>
                <pluginId>grpc-java</pluginId>
                <pluginArtifact>io.grpc:protoc-gen-grpc-java:${grpc.version}:exe:${os.detected.classifier}</pluginArtifact>
            </configuration>
            <executions>
                <execution>
                    <goals>
                        <goal>compile</goal>
                        <goal>compile-custom</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

### Time to Compile
With the mods to our `pom.xml` complete, we should be able to run a maven clean and compile.
>Note: You may need to download sources in addition to running maven compile, to make your IDE happy.

Once you have a clean compile, you should be able to expane the `target` folder and see generated sources based on your proto files. Similar to the image below:
![image tne-generated-sources.png](../assets/screenshots/tne-generated-sources.png)


## Next Up:
Now that you have successfully compiles your proto files, it is time to integrate them into our application. Continue on to [Lab 2: Hands on with gRPC](./assets/lab2-grpc.md)

#### Resources:
If you need to revisit the setup see the [README.md](../README.md)