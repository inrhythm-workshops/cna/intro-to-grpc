# Configure IntelliJ Database Connection

If you are using IntelliJ IDEA, you can use the *_Database Tools and SQL_* plugin that often comes bundled.
If for some reason it is not already installed in your version, you can install it with the IntelliJ plugin manager.

Open *_Database_* plugin and Add a connection for Postgres.

Database Connection Info can be found and modified in the file `db/flyway/flyway.conf`

Once you have the connector setup, you can click "Test Connection" to verify, if setup correctly you will see something similar to this:
![image tne-db-test-connection.png](../assets/screenshots/tne-db-test-connection.png)

After verifying you can connect, click on "Apply" and close the window. Now in the DB browser, refresh and you should
see a database named *NoteTaker*. Expand `NoteTaker > public > tables` and you should see at least four tables:
dd_types, flyway_schema_history, notes, and users

![image tne-db-table-list-v2.png](../assets/screenshots/tne-db-taable-list-v2.png)

You can verify that the tables were populated with some test data by running the following query:
```sql
SELECT *
FROM users
WHERE 1=1
ORDER BY user_name;
```

This should return a few record, such as in the image below:
![image tne-sample-users.png](../assets/screenshots/tne-sample-users.png)

