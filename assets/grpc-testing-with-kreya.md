# gRPC Testing with Kreya GUI
Kreya is a tool for testing gRPC and REST APIs that is similar to Postman or Insomnia. At the moment, Kreya seems to have a bit more support for gRPC than the other two.

## Setup an Environment
The first time you use Kreya, you should setup one or more Environments for use. For our tests, lets setup a **Local** environment.

* Browse to the _Project Tab > Environments_
* Click on "New environment"
* Under "General" tab, enter `Local` for your environment name
  * Pro Tip: You can assign a color to your environment name to easily distinguish it.
* Click on "Save"
* Click on "Back" to return to main screen

## Setup Default Settings
* Browse to the _Project Tab > Default Settings_
* Look for `Configure default settings for`  and change from "all environments" to "Local"
* Set your *Endpoint* to http://localhost:9090
* Click on "Save"
* Click on "Back" to return to main screen

## Import Proto Files
* Browse to the _Project Tab > Importers_
* Click on _proto-file_ option
* Click on "Add proto files" option
  * Select the proto file to import
* Click on "Save"
* Click on "Back" to return to main screen

## Running User Service Tests
>NOTE: Be sure that you gRPC Server is up and running, before running tests.

Assuming you have loaded up the user service proto file,expand the folders down to `user/UserSrvc".

Click on `GetAllUsers`. In the request area, make sure we have an empty JSON request, as this method does not send any data.
Click on the `Send` button to the right of the *gRPC* path bar.

You should receive results similar to:
```json
{
  "userEnvelopes": [
    {
      "userName": "The_Gunslinger",
      "userType": "USER",
      "userId": "290f36c5-0b70-402a-849c-36ee18b6c36a",
      "firstName": "Stephen",
      "lastName": "King",
      "dateCreated": "19417",
      "dateUpdated": "19417"
    },
    {
      "userName": "Captain_Ledger",
      "userType": "USER",
      "userId": "fd0d6748-f34c-4591-8271-031d5e95cac8",
      "firstName": "Jonathan",
      "lastName": "Maberry",
      "dateCreated": "19417",
      "dateUpdated": "19417"
    }
  ]
}
```
