# gRPC Server Setup
Now that we have a gRPC service or two, let's define a gRPC server so that we can listen for gRPC requests.

Please download the file [GrpcServer.java](https://gitlab.com/inrhythm-workshops/cna/intro-to-grpc/-/blob/lab2/src/main/java/com/example/tntengine/grpc/server/GrpcServer.java)
into your project and place in the `grpc.server` package.

This file defines how our gRPC server should run, what port it is on, what services the server announces to other, etc. In the interest of time, we will not go into details in this Lab. That said, we highly recommend you review and tinker the GrpcServer.

The main method of interest in the GrpcServer is this:
```java
public static void startServer() throws IOException, InterruptedException {
    logger.info("About to launch the Note Taker Engine gRPC Server. Standby.");

    if (!isReady()) {
        throw new InterruptedException("One of more of the DAOs required for gRPC Server is undefined.");
    }

    // Define our gRPC Server Instance, including services
    Server server = ServerBuilder.forPort(GRPC_SERVER_PORT)
            .addService(new NoteSrvcImpl(noteDAO)) // Our implementation of the Note Service
            .addService(new UserSrvcImpl(userDAO)) // Our implementation of the User service
            .permitKeepAliveWithoutCalls(true)
            .build();

    logger.info("Starting TNT gRPC server...");
    server.start();
    logger.info("TNT gRPC Server started!");
    server.awaitTermination();
}
```

>** WARNING **
> 
> If you plan to run this before you have created the Note Service Implementation, you will need to comment it out of the `GrpcServer` class.

### Configure AppConfig
In our AppConfig lets create a bean for our services.

```java
@Configuration
public class AppConfig {
    @Autowired
    private Environment env;

    /*** Bean Declarations ***/
    @Bean
    UserSrvcImpl userService(UserDAO userDAO) {
        return new UserSrvcImpl( userDAO );
    }

    @Bean
    NoteSrvcImpl noteService(NoteDAO noteDAO) {
        return new NoteSrvcImpl( noteDAO );
    }

}
```


Now create (or download) a file called `TntEnngineApplication`.  The basics will look similar to the code below:

```java
package com.example.tntengine;

@SpringBootApplication
public class TntEngineApplication {
    private static final Logger logger = LoggerFactory.getLogger(TntEngineApplication.class);
    private static int GRPC_SERVER_PORT = 9090;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private Environment env;


    public static void main(String[] args) {
        SpringApplication.run(TntEngineApplication.class, args);
    }

    /** Declare our Beans **/
    @Profile("!test")
    @Bean
    public CommandLineRunner demo() {

        return (Args) -> {
            logger.info("-------------------------------");
            logger.info("-- Welcome to the Note Taker --");
            logger.info("-------------------------------");

            // Check to see if we have a custom port defined
            int grpcPort = env.getProperty("grpc.server.port", Integer.class, GRPC_SERVER_PORT);
            logger.info("gRPC server port is: {}", grpcPort);

            /// Configure our gRPC Server
            // No need to instantiate an instance GrpcServer is static
            // We do need to make sure we set our DAO's
            GrpcServer.setNoteDAO(noteDAO);
            GrpcServer.setUserDAO(userDAO);
            GrpcServer.setGrpcServerPort(grpcPort);

            // Start our gRPC Server
            logger.info("Starting gRPC Server;");
            GrpcServer.startServer();
            logger.info("gRPC server started.");

            logger.info("\b------------------------------");
       };

    }
}

```

The above code will start up a server on the and keep it running in the background until we stop it. As part of this we
will provide DAO's and a what port that our app is listening on.

From IntelliJ, and most modern IDEs, you can run `TntEngineApplication` to bring up the server instance.

## Testing with Kreya GUI Client
Now that you have a gRPC server service up and running, you might want to test with a GUI client. Please see [gRPC Testing with Kreya](../assets/grpc-testing-with-kreya.md) for more details.

## Testing with a Java Client
We can also write our own gRPC client that we could use to talk to a gRPC server.


Below is an example of a client we might right to talk to the User Service we created.
```java
package com.example.tntengine.grpc.client;

import com.example.tntengine.grpc.user.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.*;

/**
 * This gRPC client is based on an example from Baeldung (https://www.baeldung.com/grpc-introduction)
 *
 * This implementation is mostly an example, akin to a test. It will lookup all users and it will lookup a single user
 */
public class UserClient {
    private static final Logger logger = LoggerFactory.getLogger(UserClient.class);
    private static int GRPC_SERVER_PORT = 9090;

    public UserClient(int grpcServerPort) {
        setGrpcServerPort(grpcServerPort);
    }

    public static void main(String[] args) {
        // In our example we will use plain text, the usePlaintext() should not be called for production applications.
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", GRPC_SERVER_PORT)
                .usePlaintext()
                .build();

        // Lookup all users
        lookupAllUsers(channel);

        // Lookup a specific user by username
        lookupUserByUsername(channel, "Duncan_Idaho");


        // Shutdown our channel
        channel.shutdown();
    }

    private static void lookupAllUsers(ManagedChannel channel) {
        // Create stub for UserSrvc
        UserSrvcGrpc.UserSrvcBlockingStub userSrvcBlockingStub = UserSrvcGrpc.newBlockingStub(channel);

        logger.info("** Looking up all users **");
        // Define our getAllUsersResponse
        GetAllUsersResponse getAllUsersResponse = userSrvcBlockingStub.getAllUsers(
                GetAllUsersRequest.newBuilder().build()
        );

        // Display all users
        if (getAllUsersResponse != null) {
            for(UserEnvelope envelope : getAllUsersResponse.getUserEnvelopesList()) {
                logger.info("User: {}", envelope);
            }
        }
    }

    private static void lookupUserByUsername(ManagedChannel channel, String username) {
        // Create stub for UserSrvc
        UserSrvcGrpc.UserSrvcBlockingStub userSrvcBlockingStub = UserSrvcGrpc.newBlockingStub(channel);

        logger.info("Lookup a single user...");
        GetUserByUsernameResponse userByUsername = userSrvcBlockingStub.getUserByUsername(
                GetUserByUsernameRequest.newBuilder().setUserName(username).build()
        );

        if (userByUsername.hasUserEnvelope()) {
            logger.info("User Envelope: \n{}", userByUsername.getUserEnvelope());
        } else {
            logger.warn("Username, {}, was not found.", username);
        }
    }

    public static void setGrpcServerPort(int port) {
        GRPC_SERVER_PORT = port;
    }
}

```