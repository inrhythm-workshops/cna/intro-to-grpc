# Lab 2, Part 2: Setting up gRPC based Note Service

In Part 1 of Lab 2, you created a gRPC based User Service and an integration test. In this lab, we will walk you through setting up a gRPC service for our Note entity.

In your IDE create a new file called `NoteSrvcImpl.java` in the package `grpc.server`.

As with the `UserSrvcImpl.java` we will create an instance of a Logger and provide a constructor to allow us to pass in our Data Access Object (DAO).
```java
package com.example.tntengine.grpc.server;

@GrpcService
public class NoteSrvcImpl extends NoteSrvcGrpc.NoteSrvcImplBase {
    private static final Logger logger = LoggerFactory.getLogger(NoteSrvcImpl.class);

    private NoteDAO noteDAO;

    public NoteSrvcImpl(NoteDAO noteDAO) {
        logger.info("NoteSrvcImpl initialized. Passed in instance of NoteDAO.");
        this.noteDAO = noteDAO;
    }
```

Let's first create a method to get our note by a note id. If you looked over our table design, you may have noticed that the NoteID is a UUID. 
Since UUID is not Protocol Buffer data type, we treat it like a string, and let our application convert it to a UUID.

Like with the UserSrvcImpl, we perform a data lookup, and if found, we return a response with one UserEnvelope.
```java
@Override
public void getNoteById(GetNoteByIdRequest request, StreamObserver<SingleNoteResponse> responseObserver) {
    Note note = noteDAO.getNoteById(UUID.fromString(request.getId()));
    String myNote = note.getNote();

    // Put our note in an envelope
    NoteEnvelope envelope = NoteEnvelope.newBuilder()
            .setNote(myNote)
            .setNoteType(note.getNoteType().getCode())
            .setId(note.getId().toString())
            .setDateCreated(note.getDateCreated().toEpochDay())
            .setDateUpdated(note.getDateUpdated().toEpochDay())
            .build();

    // Finish building our response object
    SingleNoteResponse response = SingleNoteResponse.newBuilder()
        .setNoteEnvelope(envelope)
        .build();
    
    // Return our response
    responseObserver.onNext(response);
    responseObserver.onCompleted();
}
```


To tie Notes to a User, the Note entity contains a UserID. A Note id hsd exactly on user. So let's create a method
to return all notes linked to a UserID. This logic is very similar to our method to `UserSrvcImpl#getAllUsers()`

```java
@Override
public void getNotesByUserId(GetNotesByUserIdRequest request, StreamObserver<MultiNoteResponse> responseObserver) {

    // Get our notes
    List<Note> noteList = noteDAO.getNotesByUserId( UUID.fromString(request.getUserId()) );

    // Creat an instance of our response builder
    MultiNoteResponse.Builder responseBuilder = MultiNoteResponse.newBuilder();

    for ( Note myNote : noteList ) {
        NoteEnvelope envelope = NoteEnvelope.newBuilder()
                .setId(myNote.getId().toString())
                .setNote(myNote.getNote())
                .setNoteType(myNote.getNoteType().getCode())
                .setUserId(myNote.getUserId().toString())
                .setDateCreated(myNote.getDateCreated().toEpochDay())
                .setDateUpdated(myNote.getDateUpdated().toEpochDay())
                .build();

        responseBuilder.addNoteEnvelope(envelope);
    }

    // Complete building the response
    MultiNoteResponse response = responseBuilder.build();

    // Prepare to return result
    responseObserver.onNext(response);
    responseObserver.onCompleted();
}

```

Now we should verify that we can run a Maven clean and compile with no errors.

## Next Step: Create a gRPC Server Instance
Now that we have a couple of services defined, we need to implement a gRPC server so that we can accept requests for our services.
Please continue on to [gRPC Server Setup](../assets/lab2-grpc-server-setp.md)